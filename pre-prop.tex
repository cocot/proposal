\documentclass[small]{anr}
\usepackage[ensuremath]{ebutf8}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage{tabularx}

\newcommand\NAME{Flacon}
\title{Fresh Logical Approaches for Concurrency\break
  Nouvelles approches logiques pour la concurrence}

\geometry{headsep=1.5cm,top=3.4cm}
\sethead
  {\colorbox{chapter!10!white}{%
    \begin{tabularx}{\textwidth}{@{}lXll@{}}
      \large\textcolor{chapter}{AAPG2019} & \large\bfseries\NAME & & PRC \\[.5ex]
      Coordinated by: & Emmanuel BEFFARA & 48 months & 200 000€ \\[.5ex]
      \multicolumn{4}{@{}l}{\em CES 48 --- Fondements du numérique: informatique,
        automatique, traitement du signal}
    \end{tabularx}}}{}{}%

\begin{document}
\maketitle

\section{Context, positioning and objectives}%
\label{sec:objectives}

This proposal is part of a long term project in fundamental research at
the frontier between mathematical logic and theoretical computer science, more
specifically between proof theory and the study of syntax and semantics of programming
languages. Inspired by the success of the logical approach to the
study of \emph{functional} programming languages, we want to establish logical
foundations for \emph{concurrent} programming languages. 
Such foundations are necessary as a basis for a sustainable source of
abstractions and mathematical tools to reason about concurrent programs,
scaling with the growing complexity of modern software architecture.

Our Grail is the
design of \emph{the} theoretical programming language that would be to concurrency
what the λ‑calculus is to sequential programming.
This hypothetical language would come equipped with a structured range of
semantics inspired by denotational semantics, that would intrinsically support
modular reasoning and integrate low-level operational aspects with high-level
techniques in a mathematically refined setting. This
utopic goal is a strong guideline that motivates and structures our
research efforts.

\subsection{General context}

Modern software systems, from the Web of Things to car navigation systems, are
by nature distributed, heterogeneous and evolving. This provides
tremendous power and adaptability but comes at the price of critical issues
regarding safety and predictability, and more generally with scalability
concerns.
Formal methods are used to tackle these issues as well as for implementation
and verification. In particular, modularity and
correctness are central concerns in the theory of programming languages: type
systems formalise methods of static analysis by checking or inferring
specifications of programs and guarantee the safety of
composition;
denotational semantics defines a general theory of invariants; together they
provide modular reasoning principles for correctness. Decades of work have
brought a comprehensive landscape of connected tools and results, but their
application domain is usually restricted to high-level sequential programming
languages.
For concurrent systems, the picture is rather different: complex architectures,
from multi-core processors and distributed online applications to
resource-constrained embedded systems, exhibit behaviours that have been
studied operationally on a case-by-case basis, but for which general modular methods are
lacking. For example:

\subsubsection{Specification and verification}

Compositional, scalable specification and proof methods for concurrent systems
have existed for some time, under the form of program logics (stemming from
rely/guarantee reasoning~\cite{jones-1983-tentative} or concurrent separation
logic~\cite{brookes-2016-concurrent}) or type
systems~\cite{reynolds-1978-syntactic,honda-2016-multiparty}. Such systems now
exist in a large variety, specialized for many languages or target properties.
Extending these further calls for the extraction of general principles, type
systems and logics governing interference and separation in concurrent
systems; the experience of decades of work on sequential programs tells us the
importance of tools from mathematical logic, denotational semantics and proof
theory in this endeavour (separation logic~\cite{reynolds-2002-separation},
for instance, originates from linear logic~\cite{girard-1987-linear}).

\subsubsection{Operational semantics}

Concurrent systems exist in an astonishingly large spectrum, from
multi-threaded programs to connected objects; but their operational semantics
is often described in a domain-specific fashion. There is a need for a
mathematically solid, fine-grained and syntax-free description of the
execution of concurrent systems so as to give a basis for the seamless
interfacing of systems of different scale or nature, and the specification or
certification of program transformations or optimization.

\subsubsection{Quantitative aspects}

Finally, components of concurrent programs often operate in very restricted
environments, with very tight access to resources like battery power, time,
memory, etc. Tools to specify and
study concurrent systems should hence be resource-aware. For sequential systems, recent work in type
systems~\cite{ghica-2014-bounded,petricek-2014-coeffects} and denotational
semantics~\cite{laird-2013-weighted} allows one to represent and constrain such
quantitative aspects, again relying on the tradition in proof theory and denotational semantics.

\NAME\ is part of a long-term effort to give a general unifying framework of
connected tools for concurrent systems, dealing with these issues in an
abstract manner by leveraging the power of proof theory and semantics.
We believe that significant progress is achievable in the forthcoming years, 
relying on recent breakthroughs mentioned below.

\subsection{Scientific programme}

This project's aim is to seek for a consistent ecosystem of formal tools
including denotational semantics and well-structured operational semantics,
from which a common abstract language and behavioural type systems with their
proof theory will naturally emerge, altogether serving as a foundation for
the study of concurrent computation.
Rather than encompassing the variety of existing languages for concurrent
processes in a unique one, which would be neither realistic nor even
necessarily desirable, our goal is to develop shared formalisms that would be
by design rooted in a comprehensive semantic landscape with genuinely general
results.
Our work will be structured around the following axes:

\subsubsection{Logical approaches to operational semantics}

The aim is to describe interaction at an operational level in a
way that makes abstract and compositional reasoning natural. This involves
moving beyond plain transition systems and ad-hoc operational semantics,
integrating parallelism and effects without committing to particular
operational formalisms.
A promising approach in this direction is to be found in the framework of
classical realizability, which provides new models of mathematical theories
where computational features like control and
state~\cite{krivine-2012-realizability} using subtle model-theoretic
considerations.
This theory has proved successful in studying transformations of sequential
programs and recently revealed innovative ways of describing parallel and
non-deterministic behaviour~\cite{geoffroy-2018-classical}.

\subsubsection{Proof theory and behavioural types}

The point is to understand the logic at work behind type systems for
processes in order to get logical accounts of “good” behaviours in processes.
For instance, various typing systems aim at ensuring
non-interference~\cite{reynolds-1978-syntactic} but their correctness is often
established by ad-hoc syntactic means; we will explore the possibilities
offered by the new models mentioned above to study such properties more
semantically. The proofs-as-schedules program, introduced by
Beffara~\cite{beffara-2014-proof}, relates operational and denotational
aspects from the point of view of type systems,
refining pure non-determinism by means of logical specification and
bridging the gap with behavioural type systems.

\subsubsection{Quantitative aspects in types and models}

Quantitative techniques are emerging in different areas
to face the challenges of resource-aware computation; we will
investigate such techniques for higher-order concurrent
programming languages. We expect the quantitative approach to bring a fresh
view of the computational aspects of the underlying languages.
Type systems are a promising tool in this endeavour, including
for instance
Dal Lago, Martini and Sangiorgi's light logics for higher-order
processes~\cite{dallago-2013-light} or Ghica and Smith's type system with
resources~\cite{ghica-2014-bounded}.
A related achievement in proof theory is the development of
non-uniform models of proof systems which expose
operational features in new and insightful ways, allowing for instance the
interpretation of process calculi in differential logic, as initiated by
Ehrhard and Laurent~\cite{ehrhard-2010-acyclic}.
Recent works by Beffara on order algebras~\cite{beffara-2017-order} and by
Pagani, Tasson and Vaux~\cite{pagani-2016-strong} on normalisation in
non-uniform λ-calculus are steps towards the integration of syntactic and
semantic aspects in the quantitative setting.

\subsubsection{New syntactic approaches in concurrency}

The focus is the study of
languages with an emphasis on the specific kind of rewriting theory that
concurrent semantics requires.
Proof nets in concurrent geometry of
interaction~\cite{dallago-2014-geometry} come equipped with a notion of
normalisation that deals with synchronisation and logical soundness justifies
the good behaviour of normalisation even if synchronisation can produce
deadlocks when logical consistency is not enforced.
A complementary proof-theoretical approach is the
study of reduction strategies: in the λ-calculus, head reduction provides
a bridge between the calculus and its execution via abstract
machines; we will develop similar notions for concurrent
calculi, with similarly fruitful consequences.
Many results relate calculi by means of
encodings built on the implementation of
operational intuitions and proved sound using appropriate process
equivalences. Building serious logical descriptions of processes paves the way
to a systematic study of process transformations where
logical consistency provides soundness arguments by construction.

\section{Organisation of the project}

The foundational nature of our project implies working on a long time scale,
hence the requested duration of 48 months for our project.

\subsubsection{Participants}

Our projects requires expertise on various fields of logic and semantics,
which justifies a structure around three partner sites.
\textbf{Marseille --- I2M} with
  \href{http://iml.univ-mrs.fr/~beffara/}{Emmanuel Beffara} (coordinator),
  \href{http://iml.univ-mrs.fr/~regnier/}{Laurent Regnier} and
  \href{http://iml.univ-mrs.fr/~vaux/}{Lionel Vaux};
  this team is active in the development of algebraic and
  interactive semantics of proofs,
  with a cultural background in mathematics.
\textbf{Paris --- IRIF} with
  \href{http://www.irif.fr/~ehrhard}{Thomas Ehrhard} and
  \href{http://www.irif.fr/~faggian}{Claudia Faggian}, with
  \href{https://lipn.univ-paris13.fr/~mazza/}{Damiano Mazza}
  from LIPN in Paris Nord;
  this team has expertise on a wide range of topics from denotational
  semantics (Ehrhard) to interactive models of logic (Faggian, Mazza)
  and concurrency (Mazza).
\textbf{Lyon --- LIP} with
  \href{https://perso.ens-lyon.fr/daniel.hirschkoff/}{Daniel Hirschkoff},
  \href{http://perso.ens-lyon.fr/olivier.laurent/}{Olivier Laurent} and
  \href{http://perso.ens-lyon.fr/colin.riba/}{Colin Riba}, with
  \href{http://www.lama.univ-savoie.fr/~hirschowitz}{Tom Hirschowitz}
  from LAMA in Chambéry;
  this team provides a broad expertise on logic, semantics and concurrency theory.

Beffara will act as coordinator of the project, with an involvement of 50\% of
his research time.
Each member of the team is already active on neighbouring topics and the
coordinator's role will be to foster concrete interactions among members and
outside, building on a well established
tradition of exchanges and a rich network of international collaborations.
Complementary competences in the team reflect
well the fact that our project builds on the work and results of previous
projects (as witnessed by various bibliographic references in the scientific
programme),
while opening new perspectives and directions. The individual
involvement of each member in the project is expected to be around 25\% on
average.

\subsubsection{Budget}

We plan to support a large involvement of young researchers hence a significant
budget is allocated to postdoctoral grants and missions
for exchanges of students from and towards international teams.
A priority is towards training of young researchers, allowing them to
define, start and pursue their own research programme in relation with the
objectives of the project.

We plan to organize one or two proper project meetings each year, again with a
priority given to contributions of young researchers,
plus a workshop around the middle of the
project, with an international audience and a wider scope.
Another natural meeting place for the project members and the community
of proof theory and semantics will be the CHoCoLa seminar.
Finally, research visits for members of the project and collaborators
will also play a key role in
enriching our project with new inputs as well as making our work known.

\begin{bigtable}{@{}lrrr@{\qquad}r@{}}
  \toprule
                                                 &  Marseille &      Paris &       Lyon &      Total \\ \midrule
  Student exchanges (2 000 € per month)          &   5 000.00 &   5 000.00 &   5 000.00 &  15 000.00 \\
  Visiting positions (4 000 € per month)         &   4 000.00 &   4 000.00 &   4 000.00 &  12 000.00 \\
  Missions (conferences  etc.)                   &   5 000.00 &   5 000.00 &   5 000.00 &  15 000.00 \\ \addlinespace
  Team meetings                                  &   2 500.00 &   2 500.00 &   2 500.00 &   7 500.00 \\
  Mid-term workshop                              &   6 000.00 &            &            &   6 000.00 \\
  CHoCoLa seminar                                &            &            &  15 000.00 &  15 000.00 \\ \addlinespace
  Postdoctoral contracts                         &  54 360.00 &  54 360.00 &            & 108 720.00 \\ \addlinespace
  Instruments and materials, including computers &   2 000.00 &   2 000.00 &   2 000.00 &   6 000.00 \\
  Management                                     &   6 308.80 &   5 828.80 &   2 680.00 &  14 817.60 \\ \midrule
  Total                                          &  85 168.80 &  78 688.80 &  36 180.00 & 200 037.60 \\
  \bottomrule
\end{bigtable}


\section{Impact and outcomes of the project}

All modern software systems have some degree of parallelism and concurrency.
Given the trend of development of low-level languages that explicitly include
concurrent features (like \href{https://golang.org/}{Go} or
\href{https://www.rust-lang.org/}{Rust}) and at the same time the emergence of
formally certified compilers (like in the
\href{http://compcert.inria.fr/}{CompCert} project), it is natural to imagine
certified toolchains in a concurrent context, in order to provide safe, secure
and efficient software, using efficient languages, trusted compilers and
program analysers. This long-term ambition requires significant foundational
work in which our project will contribute.

\subsubsection{Scientific impact and diffusion}

We expect our project to lead to regular publications in top international
conferences
(\href{http://lics.rwth-aachen.de/}{LICS},
\href{http://www.sigplan.org/Conferences/POPL/}{POPL},
\href{http://concur2014.org/}{Concur},
\href{http://fscd2016.dcc.fc.up.pt/}{FSCD},
\href{http://eacsl.kahle.ch/conferences.html}{CSL})
and journals
(\href{http://www.journals.elsevier.com/information-and-computation/}{I\&C},
\href{http://www.lmcs-online.org/}{LMCS},
\href{http://www.sciencedirect.com/science/journal/03043975}{TCS},
\href{http://journals.cambridge.org/action/displayJournal?jid=MSC}{MSCS})
in theoretical computer science.
Our results will also be available on open-access platforms such as
\href{http://arxiv.org/}{arXiv} or \href{https://hal.archives-ouvertes.fr/}{HAL}.

Apart from publication, we will disseminate our work through regular visits
for seminars or collaborations, both in France and abroad.
Among our international collaborators are recognized experts like
\href{http://www.cl.cam.ac.uk/~gw104/}{Glynn Winskel} in Cambridge (semantics of concurrency),
\href{http://www.doc.ic.ac.uk/~yoshida/}{Nobuko Yoshida} in London (theoretical and practical aspects of concurrency),
\href{http://www.cs.bham.ac.uk/~drg/}{Dan Ghica} in Birmingham (game semantics and its applications),
\href{http://www.cs.bath.ac.uk/~jl317/}{Jim Laird} in Bath (semantics with quantitative and concurrent features),
\href{http://users.ecs.soton.ac.uk/ps/}{Paweł Sobociński} in Southampton
(algebraic structuring of operational semantics),
\href{http://www-mmm.is.s.u-tokyo.ac.jp/~ichiro/}{Ichiro Hasuo} in Tokyo
(algebraic approaches in concurrency),
\href{http://http://www.cs.unibo.it/~dallago/}{Ugo Dal Lago} and
\href{http://www.cs.unibo.it/~sangio/}{Davide Sangiorgi} in Bologna
(concurrency, types, quantitative semantics),
\href{https://www.fing.edu.uy/~amiquel/}{Alexandre Miquel} in Montevideo
(realizability, proof theory).

The importance given to the mobility of students and young researchers is also
part of our diffusion strategy. Indeed, their interaction within our
international network and outside will help develop and spread our results
concretely and foster future developments in the aforementioned long-term
directions.


\section{Bibliography}

\bibliographystyle{abbrv}
\begin{multicols}{2}
  \footnotesize
  \bibliography{biblio}
\end{multicols}

\end{document}
