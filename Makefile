.PHONY: all
all: pre-prop.pdf projet-main.pdf projet-app.pdf

%.pdf %.aux: %.tex
	rubber -d $<

%.mark: %.aux
	sed -n 's/\\newlabel{appendix}{{[^{}]*}{\([0-9]*\)}.*/\1/p' $< > $@
	grep -q . $@

%-main.pdf: %.pdf %.mark
	pdfjam -q -o $@ $< -$(shell expr $(shell cat $*.mark) - 1)

%-app.pdf: %.pdf %.mark
	pdfjam -q -o $@ $< $(shell cat $*.mark)-

pre-prop.pdf: anr.cls
projet.pdf: anr.cls abstract.txt
