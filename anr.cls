% A LaTeX class for ANR documents

\ProvidesClass{anr}

\newif\ifdraft \draftfalse
\newif\ifsmall \smallfalse

\DeclareOption{draft}{\drafttrue}
\DeclareOption{small}{\smalltrue}
\ProcessOptions\relax

% Basic structure and packages

\ifsmall
  \LoadClass[a4paper,11pt]{article}
\else
  \LoadClass[a4paper,11pt]{report}
\fi

\RequirePackage[
  a4paper,
  margin=2cm,
  top=2.5cm,
  headheight=8mm,
  headsep=1.15cm
]{geometry}

\RequirePackage{booktabs}
\RequirePackage[final]{hyperref}
\RequirePackage{multirow}
\RequirePackage{textcomp}
\RequirePackage{xcolor}
\RequirePackage{bibentry}

\ifdraft
  \RequirePackage{showlabels}
  \renewcommand\showlabelsetlabel[1]{%
    \kern1cm\llap{\footnotesize\ttfamily\color{green!70!black}#1}}
  \showlabels{bibitem}
\fi

% Font: standard Times and Helvetica

\RequirePackage[T1]{fontenc}
\RequirePackage{txfonts}
\renewcommand\familydefault{\sfdefault}

% Extra parameters

\def\@header{Missing \texttt{\char`\\header}}
\newcommand\header[1]{\def\@header{#1}}

\def\@AAP{Name of the AAP}
\newcommand\AAP[1]{\def\@AAP{#1}}

% Title

\renewcommand\maketitle{%
	\begin{flushleft}%
	\fontsize{20pt}{24pt}\sffamily\bfseries\@title
  \ifdraft\par\textcolor{red}{DRAFT}\fi
	\end{flushleft}%
}

\RequirePackage{titlesec}

\definecolor{chapter}{rgb}{.266,.447,.768}

\setcounter{secnumdepth}{4}

% Sectioning - small version

\ifsmall

\renewcommand\thesection{\Roman{section}}
\titleformat\section
  {\fontsize{16pt}{20pt}\sffamily\bfseries\color{chapter}}%
  {\thesection.}{0.3em}{}
\titlespacing\section{0mm}{4.2mm}{2.1mm}

\titleformat\subsection
  {\sffamily\bfseries\color{chapter}}%
  {\thesubsection.}{0.3em}{}
\titlespacing\subsection{0em}{2.1mm}{1mm}

\titleformat\subsubsection[runin]{}{}{0pt}{\bfseries}[.]
\titlespacing\subsubsection{0pt}{0.5ex}{1em}

% Sectioning - large version

\else

\renewcommand\thechapter{\Roman{chapter}}
\titleformat\chapter
  {\fontsize{16pt}{20pt}\sffamily\bfseries\color{chapter}}%
  {\thechapter.}{0.3em}{}
\titlespacing\chapter{0mm}{4.2mm}{2.1mm}
\titleclass\chapter{straight}

\titleclass\subchapter{straight}[\chapter]
\newcounter{subchapter}[chapter]
\@addtoreset{section}{subchapter}
\renewcommand\thesubchapter{\thechapter.\arabic{subchapter}}
\titleformat\subchapter
  {\fontsize{14pt}{17pt}\sffamily\color{chapter}}%
  {\thesubchapter.}{0.3em}{}
\titlespacing\subchapter{0mm}{4.2mm}{2.1mm}

\renewcommand\thesection{\arabic{section}}
\titleformat\section
  {\fontsize{13pt}{15pt}\sffamily\bfseries}%
  {\thesection.}{0.3em}{}
\titlespacing\section{0mm}{4.2mm}{2.1mm}

\titleformat\subsection
  {\sffamily\bfseries}%
  {\thesubsection.}{0.3em}{}
\titlespacing\subsection{0em}{2.1mm}{1mm}

\titleformat\subsubsection
  {\fontsize{9pt}{15pt}\sffamily\bfseries}%
  {\thesubsubsection.}{0.3em}{}
\titlespacing\subsubsection{0em}{2.1mm}{1mm}

\fi

% Table of contents

\RequirePackage{titletoc}

\contentsmargin{5mm}
\dottedcontents{chapter}[6mm]{\vskip 1pt\bfseries}{6mm}{7pt}
\dottedcontents{subchapter}[12mm]{}{8mm}{7pt}
\dottedcontents{section}[14mm]{}{6mm}{7pt}
\dottedcontents{subsection}[20mm]{}{8mm}{7pt}

\renewcommand\tableofcontents{%
  \begin{list}{}{%
      \leftmargin=2cm \rightmargin=2cm
      \parsep=-1pt}%
    \item\sffamily\scshape
    \textbf{\contentsname}\par\small
    \@starttoc{toc}\par
  \end{list}}

\renewcommand\contentsname{Table of contents}

\setcounter{tocdepth}{1}

% Headers

\renewpagestyle{plain}{%
  \sethead
    {\begin{minipage}{10cm}\sffamily\slshape\@header\end{minipage}}{}%
    {\fontsize{18pt}{21pt}\sffamily\color{blue}\@AAP}%
  \setfoot{}{}{\thepage}%
}

\pagestyle{plain}

% Bibliography

\renewenvironment{thebibliography}[1]
     {\list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\endlist}

% Links

\hypersetup{
  colorlinks=true,
  linkcolor=red!50!black,
  citecolor=green!75!black
}

% Additional commands

\ifdraft
  \newenvironment{instructions}%
    {\begin{list}{}{\leftmargin=1cm}\slshape\color{gray}\item}%
    {\end{list}}
\else
  \RequirePackage{verbatim}
  \newenvironment{instructions}{\comment}{\endcomment}
\fi

\newenvironment{todo}%
  {\begin{list}{}{\leftmargin=1cm}\color{red}\item}%
  {\end{list}}

\newenvironment{bigtable}[2][]
  {\begin{center}\small\noindent#1\begin{tabular}{#2}}%
  {\end{tabular}\end{center}}

\RequirePackage{enumitem}

\setlist{leftmargin=1.5em,
  topsep=0.5ex,itemsep=0.5ex,parsep=0ex}
\newlist{topics}{enumerate*}{1}
\setlist[topics]{resume,label=\textbf{(\arabic*)},ref=\arabic*}

\newenvironment{tasks}{%
  \titleformat\section
    {\fontsize{13pt}{15pt}\sffamily\bfseries}%
    {\textsc{Task} \thesection.}{0.3em}{}%
}{}
